//
//  OnLiveEditTextField.m
//  STeBook
//
//  Created by huangfei on 2017/8/15.
//  Copyright © 2017年 rain. All rights reserved.
//

#import "CustomTextfield.h"

@implementation CustomTextfield

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:31/255.0 green:31/255.0 blue:31/255.0 alpha:0.5];
        self.layer.cornerRadius = 16;
        self.layer.masksToBounds = YES;
        NSString *holderText = @"红色的placeholder";
        NSMutableAttributedString *placeholder = [[NSMutableAttributedString alloc] initWithString:holderText];
        [placeholder addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, holderText.length)];
        [placeholder addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, holderText.length)];
        self.attributedPlaceholder = placeholder;
        [self setLeftViewWithTextField:self imageName:@"icon_onlive_editBtn_normal"];
        [self setRightViewWithTextField:self imageName:@"icon_onlive_editBtn_normal"];
    }
    return self;
}

/**
 * 设置 textfield 左侧图片
 */
- (void)setLeftViewWithTextField:(UITextField *)textField imageName:(NSString *)imageName {
    
    UIImageView *leftView = [[UIImageView alloc] init];
    leftView.image = [UIImage imageNamed:imageName];
    leftView.frame = CGRectMake(0,0,16,16);
    leftView.contentMode = UIViewContentModeCenter;
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

/**
 * 设置 textfield 右侧图片
 */
- (void)setRightViewWithTextField:(UITextField *)textField imageName:(NSString *)imageName {
    
    UIImageView *rightView = [[UIImageView alloc] init];
    rightView.image = [UIImage imageNamed:imageName];
    rightView.frame = CGRectMake(0,0,16,16);
    rightView.contentMode = UIViewContentModeCenter;
    textField.rightView = rightView;
    textField.rightViewMode = UITextFieldViewModeAlways;
}

#pragma mark - 重写方法
/**
 * 返回placeholderLabel的bounds，改变返回值，调整placeholderLabel的位置
 */
- (CGRect)placeholderRectForBounds:(CGRect)bounds {
//    return CGRectMake(37, 0 , self.bounds.size.width - 37*2, self.bounds.size.height);
    return CGRectMake(37, 0, self.bounds.size.width - 37, self.bounds.size.height);
}

/**
 * 调整placeholder在placeholderLabel中绘制的位置以及范围
 */
-(void)drawPlaceholderInRect:(CGRect)rect {
//    [super drawPlaceholderInRect:CGRectMake(0, 0 , self.bounds.size.width - 37*2, self.bounds.size.height)];
    [super drawPlaceholderInRect:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
}

// 上面这两个方法会令输入前的缩进距离失效

///**
// * 控制还未输入时文本的位置，缩进12
// */
//- (CGRect)textRectForBounds:(CGRect)bounds {
//    return CGRectInset(bounds, 12, 0);
//}

///**
// * 控制输入后文本的位置，缩进12
// */
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 80, 0);
}

/**
 * 控制文本框左侧图片的位置,缩进12
 */
- (CGRect)leftViewRectForBounds:(CGRect)bounds {
    CGRect rect = [super leftViewRectForBounds:bounds];
    return CGRectOffset(rect, 12, 0);
}

/**
 * 控制文本框右侧图片的位置,缩进12
 */
-(CGRect)rightViewRectForBounds:(CGRect)bounds {
    CGRect rect = [super rightViewRectForBounds:bounds];
    return CGRectOffset(rect, -12, 0);
}

@end
