//
//  ViewController.m
//  testTextfield
//
//  Created by 圣才电子书6号 on 2017/8/23.
//  Copyright © 2017年 圣才电子书6号. All rights reserved.
//

#import "ViewController.h"
#import "CustomTextfield.h"
@interface ViewController ()

@property (nonatomic, strong) CustomTextfield *editTextfield;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:self.editTextfield];
    
}

- (CustomTextfield *)editTextfield {
    if (!_editTextfield) {
        _editTextfield = [[CustomTextfield alloc] initWithFrame:CGRectMake(50, 100, 250, 32)];
    }
    return _editTextfield;
}

@end
