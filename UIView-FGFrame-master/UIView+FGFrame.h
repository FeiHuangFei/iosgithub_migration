//
//  UIView+FGFrame.h
//  百思项目
//
//  Created by huangfei on 17/3/2.
//  Copyright © 2017年 huangfei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FGFrame)

@property (nonatomic, assign) CGFloat fg_width;
@property (nonatomic, assign) CGFloat fg_height;
@property (nonatomic, assign) CGFloat fg_x;
@property (nonatomic, assign) CGFloat fg_y;
@property (nonatomic, assign) CGFloat fg_centerX;
@property (nonatomic, assign) CGFloat fg_centerY;

@property (nonatomic, assign) CGFloat fg_right;
@property (nonatomic, assign) CGFloat fg_bottom;

@end
