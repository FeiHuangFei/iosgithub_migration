//
//  UIView+FGFrame.m
//  百思项目
//
//  Created by huangfei on 17/3/2.
//  Copyright © 2017年 huangfei. All rights reserved.
//

#import "UIView+FGFrame.h"

@implementation UIView (FGFrame)

- (CGFloat)fg_width
{
    return self.frame.size.width;
}

- (CGFloat)fg_height
{
    return self.frame.size.height;
}

- (void)setFg_width:(CGFloat)fg_width
{
    CGRect frame = self.frame;
    frame.size.width = fg_width;
    self.frame = frame;
}

- (void)setFg_height:(CGFloat)fg_height
{
    CGRect frame = self.frame;
    frame.size.height = fg_height;
    self.frame = frame;
}

- (CGFloat)fg_x
{
    return self.frame.origin.x;
}

- (void)setFg_x:(CGFloat)fg_x
{
    CGRect frame = self.frame;
    frame.origin.x = fg_x;
    self.frame = frame;
}

- (CGFloat)fg_y
{
    return self.frame.origin.y;
}

- (void)setFg_y:(CGFloat)fg_y
{
    CGRect frame = self.frame;
    frame.origin.y = fg_y;
    self.frame = frame;
}

- (CGFloat)fg_centerX
{
    return self.center.x;
}

- (void)setFg_centerX:(CGFloat)fg_centerX
{
    CGPoint center = self.center;
    center.x = fg_centerX;
    self.center = center;
}

- (CGFloat)fg_centerY
{
    return self.center.y;
}

- (void)setFg_centerY:(CGFloat)fg_centerY
{
    CGPoint center = self.center;
    center.y = fg_centerY;
    self.center = center;
}

- (CGFloat)fg_right
{
    //    return self.fg_x + self.fg_width;
    return CGRectGetMaxX(self.frame);
}

- (CGFloat)fg_bottom
{
    //    return self.fg_y + self.fg_height;
    return CGRectGetMaxY(self.frame);
}

- (void)setFg_right:(CGFloat)fg_right
{
    self.fg_x = fg_right - self.fg_width;
}

- (void)setFg_bottom:(CGFloat)fg_bottom
{
    self.fg_y = fg_bottom - self.fg_height;
}

@end
