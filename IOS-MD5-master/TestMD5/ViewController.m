//
//  ViewController.m
//  TestMD5
//
//  Created by huangfei on 2017/10/11.
//  Copyright © 2017年 huangfei. All rights reserved.
//

#import "ViewController.h"
#import "NSString+MD5.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *str = @"123456789";
    NSString *md5Result = [str md5];
    NSLog(@"%@",md5Result);
}

@end
